package DigitalHome.CheckFaceAndRecognize;

import DigitalHome.CheckFaceAndRecognize.engine.RecognizationEngine;
import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInCommon.streamers.IPCamVideoStreamer;
import DigitalHome.CheckInCommon.streamers.IVideoStreamer;
import DigitalHome.CheckInCommon.streamers.WebCamVideoStreamer;

public class CheckFaceAndRecognizeMain 
{
	// Configuration file name for this class
	private static final String CONFIG_FILE_NAME = "CheckFaceAndRecognizeMain";	
	
	public static void main(String[] args) {   

		// Get configuration
		// ----------------------------------------
		Configuration config = null;
		
		try {
			config = new Configuration(CONFIG_FILE_NAME);
		} catch (Exception e) {
			System.out.println("CONFIG ERR - " +e.getMessage());
			System.exit(-1);
		}
		
		String streamerIPCAMBonjourName = config.getProperties().getProperty("streamerIPCAMBonjourName");
		String streamerIPCAMAddress = config.getProperties().getProperty("streamerIPCAMAddress");
		int streamerStreamFormat = Integer.parseInt(config.getProperties().getProperty("streamerStreamFormat"));
		int streamerCaptureWidth = Integer.parseInt(config.getProperties().getProperty("streamerCaptureWidth"));
		int streamerCaptureHeight = Integer.parseInt(config.getProperties().getProperty("streamerCaptureHeight"));
		int streamerMinCaptureSize = Integer.parseInt(config.getProperties().getProperty("streamerMinCaptureSize"));
		boolean streamerCorrectEqualizeHist = Boolean.parseBoolean(config.getProperties().getProperty("streamerCorrectEqualizeHist"));
		int streamerFramesToSkip = Integer.parseInt(config.getProperties().getProperty("streamerFramesToSkip"));
		
		String streamerCamMode = config.getProperties().getProperty("streamerCamMode");		
		

		// Create the video Streamer
		// ----------------------------------------		
		IVideoStreamer streamer = null;
		try {
			if (streamerCamMode.equals("IPCAM")) streamer = new IPCamVideoStreamer(streamerIPCAMBonjourName, streamerIPCAMAddress, streamerStreamFormat, streamerCaptureWidth, streamerCaptureHeight, streamerCorrectEqualizeHist, streamerMinCaptureSize, streamerFramesToSkip);
			if (streamerCamMode.equals("WEBCAM_0")) streamer = new WebCamVideoStreamer(0, streamerCaptureWidth, streamerCaptureHeight, streamerCorrectEqualizeHist, streamerMinCaptureSize, streamerFramesToSkip);
			if (streamerCamMode.equals("WEBCAM_1")) streamer = new WebCamVideoStreamer(1, streamerCaptureWidth, streamerCaptureHeight, streamerCorrectEqualizeHist, streamerMinCaptureSize, streamerFramesToSkip);
			if (streamer == null) throw new Exception("Unrecognized camera mode");
		} catch (Exception e) {
			System.out.println("ERROR OPENING STREAM - " + e.getMessage());
			System.exit(-1);
		}
		
		// Create the recon engine
		// ----------------------------------------		
		RecognizationEngine engine = null;
		
		try {
			engine = new RecognizationEngine(streamer);
		} catch (Exception e) {
			System.out.println("ERROR RECON ENGINE - " + e.getMessage());
			System.exit(-1);
		}

		// Start threads
		// ----------------------------------------		
		Thread streamerThread = new Thread(streamer);
		Thread engineThread = new Thread(engine);

		try {
			// Start streamer 
			streamerThread.start();

			// Start engine			
			engineThread.start();

			// Wait for engine to end
			engineThread.join();
		} catch (InterruptedException e) {
			System.out.println("RECON ENGINE - Interrupted");
			e.printStackTrace();
		} finally {
			
			// Close the streamer when finished			
			streamer.stop();
		}
	}
}
