package DigitalHome.CheckFaceAndRecognize.engine;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map.Entry;

import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInCommon.databaseClient.CassandraClient;
import DigitalHome.CheckInCommon.restClient.FaceReconRestClient;
import DigitalHome.CheckInCommon.restClient.HomeGatewayClient;
import DigitalHome.CheckInCommon.streamers.IVideoStreamer;
import DigitalHome.CheckInCommon.transport.DetectedImage;
import DigitalHome.CheckInCommon.transport.ResponseRecognize;
import DigitalHome.CheckInCommon.transport.ResponseRecognizeScore;
import DigitalHome.CheckInCommon.transport.UserData;

public class RecognizationEngine implements Runnable {

	// Configuration file name for this class.
	private static final String CONFIG_FILE_NAME = "RecognizationEngine";	
	
	// Properties loaded from CONFIG file	
	private int _squaredThumbSize;
	private Double _minPercentageConfidence;
	private int _maxNumFramesWithoutLastRecon;
	private int _msDelayBetweenRecon;
	
	private FaceReconRestClient _restClient;
	private IVideoStreamer _streamer;
	private CassandraClient _cassandraClient;
	
	private boolean _isRunning;
	
	private UserData _lastRecognizedUser;
	private int _numFrameWithoutLastRecognizedUser;
	
    private void configure() throws IOException{
    	
		// Get the configurator
		Configuration config = new Configuration(CONFIG_FILE_NAME);
		
		_squaredThumbSize = Integer.parseInt(config.getProperties().getProperty("squaredThumbSize"));
		_minPercentageConfidence = Double.parseDouble(config.getProperties().getProperty("minPercentageConfidence"));
		_maxNumFramesWithoutLastRecon = Integer.parseInt(config.getProperties().getProperty("maxNumFramesWithoutLastRecon"));
		_msDelayBetweenRecon = Integer.parseInt(config.getProperties().getProperty("msDelayBetweenRecon"));
    }	
	
	public RecognizationEngine(IVideoStreamer streamer) throws IOException{
		
		// Configure from configuration file
		configure();
		
		_streamer = streamer;
		_isRunning = true;
		_restClient = new FaceReconRestClient();
		_cassandraClient = new CassandraClient();
	}
	
	public void stop(){
		_cassandraClient.close();
		_streamer.stop();		
		_isRunning = false;
	}

	private void searchCurrentUser(){

		boolean isLastRecognizedUserFound = false;
		ResponseRecognize reconObj = null;
		
		// Get the current detected faces
		ArrayList<Entry<BufferedImage, Double>> detectedFaces = _streamer.getAllDetectedFaces(_squaredThumbSize);			
		
		// Loop all images to recognize them one by one
		for(Entry<BufferedImage, Double> currentFace : detectedFaces) {

			try {

				// Send the image to recognize engine
				DetectedImage img = new DetectedImage(currentFace.getKey());
				reconObj = _restClient.recognizeDetectedImage(img);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			// If recognize engine returned at least one match I need to check all
			if(reconObj!=null){
				// Get all results (collection is already sorted)
				ArrayList<ResponseRecognizeScore> reconResults = reconObj.getResults(_minPercentageConfidence);

				for(ResponseRecognizeScore currResult : reconResults) {
					if(currResult.getId().equals(_lastRecognizedUser.getId())){
						isLastRecognizedUserFound = true;
						break;
					}
				}
			}
			
			// If the last recognized user is found I don't need to go on with search
			if (isLastRecognizedUserFound) break;
		}

		// If last recognized user has been found again I set the counter of missed to 0
		// otherwise I increase it.
		if (isLastRecognizedUserFound){
			_numFrameWithoutLastRecognizedUser = 0;
			System.out.println("Current user found");			
		} else {
			_numFrameWithoutLastRecognizedUser++;
			System.out.println("User not found " + _numFrameWithoutLastRecognizedUser + " time.");			
		}
		
	}
	
	private void recognizeNewCurrentUser() throws Exception{

		String newCandidateID = null;
		Double newCandidateScore = 0.0;
		
		// Clear current user
		_lastRecognizedUser = null;
		_numFrameWithoutLastRecognizedUser = 0;		
		
		System.out.println(new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()) + " - Searching for new user.");		
		
		// Get the current detected faces
		ArrayList<Entry<BufferedImage, Double>> detectedFaces = _streamer.getAllDetectedFaces(_squaredThumbSize);		
		
		// Loop all images to recognize them one by one
		for(Entry<BufferedImage, Double> currentFace : detectedFaces) {

			try {
				
				// Send the image to recognize engine
				DetectedImage img = new DetectedImage(currentFace.getKey());
				ResponseRecognize reconObj = _restClient.recognizeDetectedImage(img);
				
				// If recognize engine returned at least 
				if(reconObj!=null){
					
					// Get all results (collection is already sorted)
					ArrayList<ResponseRecognizeScore> reconResults = reconObj.getResults(_minPercentageConfidence);
					
					// I take the first result and check it against best match
					if(reconResults.size() > 0 && reconResults.get(0).getScore() * currentFace.getValue() > newCandidateScore){
						newCandidateID = reconResults.get(0).getId();
						newCandidateScore = reconResults.get(0).getScore() * currentFace.getValue();	
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//If I found a new candidate I set it
		if (newCandidateID != null){
			
			System.out.println("Recognized user with UUID: " + newCandidateID + ". Searching Cassandra for user name and preferences.");
			
			try {
				_cassandraClient.connect();
			
				_lastRecognizedUser = _cassandraClient.getUserDataByID(newCandidateID);
				
				// Check if I got data from Cassandra
				if(_lastRecognizedUser == null) {
					System.out.println("User not found in cassandra DB");
				}
			} catch (Exception e) {
				System.out.println("Error retrieving data from cassandra DB");
				throw e;
			}
		}
	}

	private void resetHomeKit() throws Exception{
		System.out.println("-----------------------------------------");
		System.out.println("RESETTING HOME KIT");
		System.out.println("-----------------------------------------");
		
		// Setting environment
		HomeGatewayClient client = new HomeGatewayClient();
		client.checkOut();
		
		// Wait two seconds to let kit reset
		try {
			System.out.println("Wait 2 seconds for the kit to reset");
			Thread.sleep(2000);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
	}	
	
	private void setPreferencesToHomeKit() throws Exception{
		System.out.println("------------------------------------------");
		System.out.println("USER RECOGNIZED");
		System.out.println("Name: " + _lastRecognizedUser.getName());
		System.out.println("Color: " + _lastRecognizedUser.getPref_color());
		System.out.println("Temperature: " + _lastRecognizedUser.getPref_temperature());
		System.out.println("Music: " + _lastRecognizedUser.getPref_music());
		System.out.println("------------------------------------------");
		
		// Setting environment
		HomeGatewayClient client = new HomeGatewayClient();
		client.setUserName(_lastRecognizedUser.getName());
		client.setTemperature(_lastRecognizedUser.getPref_temperature());
		client.setLight(_lastRecognizedUser.getPref_color());
	}
	
	public void run() {
		
		// Reset Home kit at the beginning to have a clean situation
		try {
			resetHomeKit();
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		}

		// If the grabber is not running I wait for initialization
		System.out.println("Waiting for grabber initialization");
		while(!_streamer.isRunning()) {
			try {
				Thread.sleep(1000);
				System.out.println("Waiting for grabber initialization");				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Grabber initialized");
		
    	try {		
			// While the application should run, I get images
			while(_isRunning){
				
				// If the streamer is no more running I track it
				if (!_streamer.isRunning()) {
					System.out.println("Connection to streamer lost. Exiting.");
					this._isRunning = false;
					continue;
				}
				
				try {
					// Give detected faces to engine to recognize them and get current user.
					if(_lastRecognizedUser == null ||_numFrameWithoutLastRecognizedUser > _maxNumFramesWithoutLastRecon){
						
						// If the last user was not null, I reset the HomeKit
						if(_lastRecognizedUser != null) {
							resetHomeKit();
						}
						
						// Remove current user recognized and sdearch for a new one
						_lastRecognizedUser = null;
						recognizeNewCurrentUser();
						
						// If a new user has been found I set preferences
						if(_lastRecognizedUser != null) {
							setPreferencesToHomeKit();
						}
					}else {
						searchCurrentUser();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
	    		// Wait between the two recognitions
				Thread.sleep(_msDelayBetweenRecon);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			stop();
		}
    	
    	System.out.println("Process exited.");
	
	}
}
